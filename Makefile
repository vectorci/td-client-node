all:
	npm install

.PHONY: test clean
test:
	./node_modules/.bin/mocha

circleci:
	NODE_ENV=test ./node_modules/.bin/mocha --require blanket --reporter mocha-lcov-reporter | ./node_modules/coveralls/bin/coveralls.js
	./node_modules/.bin/jsdoc -d ${CIRCLE_ARTIFACTS}/apidoc -R ./README.md ./lib/index.js

clean:
	rm -rf ./docs node_modules ./test/coverage.html

site: ./lib/index.js
	./node_modules/.bin/jsdoc -d ./docs -R ./README.md ./lib/index.js
